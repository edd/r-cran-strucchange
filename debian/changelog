strucchange (1.5-4-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version
  * debian/control: Switch to virtual debhelper-compat (= 13)

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 04 Sep 2024 06:26:55 -0500

strucchange (1.5-3-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version
  * debian/control: Switch to virtual debhelper-compat (= 11)
  * debian/compat: Removed

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 15 Jun 2022 07:08:50 -0500

strucchange (1.5-2-1+1) unstable; urgency=medium

  * No changes but source-only upload

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 18 Oct 2019 11:55:24 -0500

strucchange (1.5-2-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

  * debian/control: Change Architecture: to 'any'

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 12 Oct 2019 16:03:35 -0500

strucchange (1.5-1-3) unstable; urgency=medium

  * Rebuilding for R 3.5.0 transition
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Build-Depends: to 'debhelper (>= 10)'
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Depends: on ${misc:Depends}
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/compat: Increase level to 9
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem
  * debian/README.source: Added

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 05 Jun 2018 06:28:42 -0500

strucchange (1.5-1-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild for r-api-3.4 transition

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 30 Sep 2017 09:27:58 +0200

strucchange (1.5-1-2) unstable; urgency=medium

  * debian/compat: Created
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 25 Jun 2016 07:35:26 -0500

strucchange (1.5-1-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version 
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 07 Jun 2015 09:44:18 -0500

strucchange (1.5-0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version 
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 28 Oct 2013 20:57:10 -0500

strucchange (1.4-7-2) unstable; urgency=low

  * debian/control: Set Build-Depends: to current R version 
  * debian/control: Set Standards-Version: to current version 
  
  * (Re-)building with R 3.0.0

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 05 Apr 2013 09:19:54 -0500

strucchange (1.4-7-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Change Depends to ${R:Depends}
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 31 May 2012 06:38:44 -0500

strucchange (1.4-6-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 11 Aug 2011 10:40:32 -0500

strucchange (1.4-5-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 25 Jul 2011 20:52:58 -0500

strucchange (1.4-4-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 13 May 2011 15:36:14 -0500

strucchange (1.4-3-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 14 Dec 2010 06:59:48 -0600

strucchange (1.4-2-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Standards-Version: to current version 
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 21 Nov 2010 10:15:44 -0600

strucchange (1.4-1-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 28 Jul 2010 15:53:22 -0500

strucchange (1.4-0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 16 Feb 2010 20:10:38 -0600

strucchange (1.3-7-2) unstable; urgency=low

  * Rebuilt for R 2.10.0 to work with new R-internal help file conversion

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  
  * debian/control: Changed Section: to section 'gnu-r'

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 03 Nov 2009 05:10:52 -0600

strucchange (1.3-7-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 17 Feb 2009 12:39:07 -0600

strucchange (1.3-6-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  
 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 05 Feb 2009 21:37:19 -0600

strucchange (1.3-5-1) unstable; urgency=low

  * New upstream release

  * debian/control: Updated (Build-)Depends on current R version
  * debian/control: Updated Standards-Version: to current version
  
 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 09 Dec 2008 19:45:16 -0600

strucchange (1.3-4-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 04 Oct 2008 21:31:49 -0500

strucchange (1.3-3-1) unstable; urgency=low

  * New upstream release

  * debian/control: Build-Depends: updated to r-base-dev (>= 2.7.0)
  
 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 23 Apr 2008 20:07:00 -0500

strucchange (1.3-2-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Build-Depends: updated to r-base-dev (>= 2.5.0~20070412-1)

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 18 Apr 2007 22:08:46 -0500

strucchange (1.3-1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed,  4 Oct 2006 17:27:05 -0500

strucchange (1.3-0-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Updated Depends: + Build-Depends: to R 2.3.1
  * debian/watch: Updated regular expression 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 11 Aug 2006 20:06:33 -0500

strucchange (1.2.13-1) unstable; urgency=low

  * New upstream release

  * debian/rules: Simplified to cdbs-based one-liner sourcing r-cran.mk 
  * debian/control: Hence Build-Depends: updated to r-base-dev (>= 2.3.0)
  
  * debian/control: Standards-Version: increased to 3.7.2

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 23 May 2006 21:36:36 -0500

strucchange (1.2.12-1) unstable; urgency=low

  * New upstream release

  * debian/post{inst,rm}: No longer call R to update html index
  
 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 19 Jan 2006 13:48:49 -0600

strucchange (1.2.11-1) unstable; urgency=low

  * New upstream release
  * debian/watch: Corrected regular expression (thanks, Rafael Laboissier)
  * debian/post{inst,rm}: Call /usr/bin/R explicitly (thanks, Kurt Hornik)
  * debian/control: Upgraded Standards-Version: to 3.6.2.1

 -- Dirk Eddelbuettel <edd@debian.org>  Thu,  8 Sep 2005 23:39:10 -0500

strucchange (1.2.10-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 27 Apr 2005 22:04:41 -0500

strucchange (1.2.9-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed,  6 Apr 2005 21:02:21 -0500

strucchange (1.2.8-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 21 Jan 2005 20:41:22 -0600

strucchange (1.2.7-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue,  9 Nov 2004 21:03:22 -0600

strucchange (1.2.6-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed,  3 Nov 2004 20:17:51 -0600

strucchange (1.2.5-1) unstable; urgency=low

  * Initial Debian release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 27 Oct 2004 22:50:46 -0500


