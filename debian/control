Source: strucchange
Section: gnu-r
Priority: optional
Maintainer: Dirk Eddelbuettel <edd@debian.org>
Build-Depends: debhelper-compat (= 13), r-base-dev (>= 4.4.1), dh-r, r-cran-zoo, r-cran-sandwich
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/edd/r-cran-strucchange
Vcs-Git: https://salsa.debian.org/edd/r-cran-strucchange.git
Homepage: https://cran.r-project.org/package=strucchange

Package: r-cran-strucchange
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${R:Depends}, r-cran-zoo, r-cran-sandwich
Description: GNU R package for structural change regression estimation
 This package functions for testing, dating and monitoring of
 structural change in linear regression relationships. The strucchange
 package features tests/methods from the generalized fluctuation test
 framework as well as from the F test (Chow test) framework. This
 includes methods to fit, plot and test fluctuation processes (e.g.,
 CUSUM, MOSUM, recursive/moving estimates) and F statistics,
 respectively.  It is possible to monitor incoming data online using
 fluctuation processes.
 .
 Finally, the breakpoints in regression models with structural changes
 can be estimated together with confidence intervals.  Emphasis is
 always given to methods for visualizing the data.

